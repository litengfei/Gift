//
//  Utils.swift
//  Git@OSC
//
//  Created by AeternChan on 5/26/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Timepiece
import ReactiveCocoa


extension UIColor {
    
    class func colorFromHex(hex: NSInteger, alpha: CGFloat = 1) -> UIColor {
        
        return UIColor(red: CGFloat((hex & 0xFF0000) >> 16) / 255,
                     green: CGFloat((hex & 0x00FF00) >> 8) / 255,
                      blue: CGFloat(hex & 0x0000FF) / 255,
                     alpha: alpha)
    }
    
    class func themeBlueColor() -> UIColor {
        
        return colorFromHex(0x056BC3)
    }
    
}


extension UIView {
    func setBorder(width: CGFloat, color: UIColor) {
        layer.borderWidth = width
        layer.borderColor = color.CGColor
    }
}


extension String {
    
    subscript(i: Int) -> Character {
        return self[startIndex.advancedBy(i)]
    }
    
    subscript(range: Range<Int>) -> String {
        return self[startIndex.advancedBy(range.startIndex)..<startIndex.advancedBy(range.endIndex)]
    }
    
    
    func escapeHTMLTag() -> String {
        
        var newString = self
        let char_dictionary = [
            "&amp;": "&",
            "&lt;": "<",
            "&gt;": ">",
            "&quot;": "\"",
            "&apos;": "'"
        ];
        for (escaped_char, unescaped_char) in char_dictionary {
            newString = newString.stringByReplacingOccurrencesOfString(unescaped_char, withString: escaped_char, options: .RegularExpressionSearch, range: nil)
        }
        return newString
    }
    
    
    func md5() -> String! {
        let str = self.cStringUsingEncoding(NSUTF8StringEncoding)
        let strLen = CUnsignedInt(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
        let digestLen = Int(CC_MD5_DIGEST_LENGTH)
        let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)
        
        CC_MD5(str!, strLen, result)
        
        let hash = NSMutableString()
        for i in 0..<digestLen {
            hash.appendFormat("%02x", result[i])
        }
        
        result.destroy()
        
        return String(format: hash as String)
    }
}


extension NSDate {
    
    class func dateFromString(string: String) -> NSDate? {
        return NSDateFormatter.sharedInstance.dateFromString(string)
    }
    
    func prettify() -> String {
        var format: String
        
        if self < 1.week.ago {
            format = "yyyy-M-dd"
        } else if self < 2.days.ago {
            let days = Int((NSDate() - self) / 86400)
            format = "\(days)天前"
        } else if self < 1.day.ago {
            format = "昨天HH:mm"
        } else {
            format = "HH:mm"
        }
        
        return self.stringFromFormat(format)
    }
}


extension NSDateFormatter {
    
    class var sharedInstance: NSDateFormatter {
        struct Static {
            static let instance: NSDateFormatter = {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                return dateFormatter
                }()
        }
        return Static.instance
    }
    
}


extension UITextField {
    func rac_textSignalProducer() -> SignalProducer<String, NSError> {
        return
            self.rac_textSignal().toSignalProducer()
                .map { $0 as! String }
    }
}