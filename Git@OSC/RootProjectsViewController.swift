//
//  RootProjectsViewController.swift
//  Git@OSC
//
//  Created by AeternChan on 12/10/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit

class RootProjectsViewController: PagingViewController {
    
    @IBOutlet var button1: UIButton!
    @IBOutlet var button2: UIButton!
    @IBOutlet var button3: UIButton!
    
    var buttons: [UIButton]!
    
    let activeColor: [CGFloat] = [0/255, 122/255, 255/255]
    let negativeColor: [CGFloat] = [170/255, 170/255, 170/255]
    
    override func awakeFromNib() {
        
        let storyboard = UIStoryboard(name: "Projects", bundle: nil)
        let vc1 = storyboard.instantiateInitialViewController() as! ProjectsViewController
        let vc2 = storyboard.instantiateInitialViewController() as! ProjectsViewController
        let vc3 = storyboard.instantiateInitialViewController() as! ProjectsViewController
        
        vc1.group = .Recommended
        vc2.group = .Popular
        vc3.group = .RecentUpdated
        
        buttons = [button1, button2, button3]
        
        viewControllers.appendContentsOf([vc1, vc2, vc3])
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        for (i, button) in buttons.enumerate() {
            if i == 0 {
                button.setTitleColor(UIColor(rgba: "#007AFF"), forState: .Normal)
                button.transform = CGAffineTransformMakeScale(1.2, 1.2);
            } else {
                button.setTitleColor(UIColor(rgba: "#AAAAAA"), forState: .Normal)
                button.transform = CGAffineTransformIdentity
            }
            
            button.tag = i
            button.addTarget(self, action: "onTapTitleButton:", forControlEvents: .TouchUpInside)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: configure title buttons
    
    func onTapTitleButton(aButton: UIButton) {
        contentScrollView.setContentOffset(CGPoint(x: CGFloat(aButton.tag) * view.bounds.width, y: 0), animated: false)
        
        for (i, button) in buttons.enumerate() {
            if i == aButton.tag {
                button.setTitleColor(UIColor(rgba: "#007AFF"), forState: .Normal)
                button.transform = CGAffineTransformMakeScale(1.2, 1.2)
            } else {
                button.setTitleColor(UIColor(rgba: "#AAAAAA"), forState: .Normal)
                button.transform = CGAffineTransformIdentity
            }
        }
    }
    
    
    // MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {        
        let offsetX = contentScrollView.contentOffset.x
        let xUpLimit = scrollView.contentSize.width - scrollView.bounds.width
        
        if offsetX > 0 && offsetX < xUpLimit {
        
            let index = Int(offsetX / view.bounds.width + 0.5)
            let toIndex = offsetX > CGFloat(index) * view.bounds.width ? index + 1 : index - 1
            var offsetRatio = offsetX % view.bounds.width / view.bounds.width
            
            if index > toIndex && offsetRatio != 0 {offsetRatio = 1 - offsetRatio}
            
            UIView.transitionWithView(buttons[index], duration: 0.1, options: .TransitionCrossDissolve, animations: { () -> Void in
                self.buttons[index].setTitleColor(self.color(offsetRatio, active: true) , forState: .Normal)
                
                self.buttons[index].transform = CGAffineTransformMakeScale(1 + 0.2 * (1-offsetRatio), 1 + 0.2 * (1-offsetRatio))
                }) {_ in }
            
            UIView.transitionWithView(buttons[toIndex], duration: 0.1, options: .TransitionCrossDissolve, animations: { () -> Void in
                self.buttons[toIndex].setTitleColor(self.color(offsetRatio, active: false), forState: .Normal)
                
                self.buttons[toIndex].transform = CGAffineTransformMakeScale(1 + 0.2 * offsetRatio, 1 + 0.2 * offsetRatio)
                }) {_ in }
        }
    }
    
    
    func color(ratio: CGFloat, active: Bool) -> UIColor {
        var r, g, b: CGFloat
        
        if active {
            r = activeColor[0] + (negativeColor[0] - activeColor[0]) * ratio
            g = activeColor[1] + (negativeColor[1] - activeColor[1]) * ratio
            b = activeColor[2] + (negativeColor[2] - activeColor[2]) * ratio
        } else {
            r = negativeColor[0] + (activeColor[0] - negativeColor[0]) * ratio
            g = negativeColor[1] + (activeColor[1] - negativeColor[1]) * ratio
            b = negativeColor[2] + (activeColor[2] - negativeColor[2]) * ratio
        }
        
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
