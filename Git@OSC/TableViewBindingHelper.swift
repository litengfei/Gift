//
//  TableViewBindingHelper.swift
//  Git@OSC
//
//  Created by AeternChan on 5/31/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import ReactiveCocoa

class TableViewBindingHelper: NSObject {
    
    var tableView: UITableView = UITableView()
    var refreshControl: UIRefreshControl?
    
    init(tableView: UITableView, refreshControl: UIRefreshControl?, sourceSingnal source: SignalProducer<Int, ReactiveCocoa.NoError>) {
        super.init()
        
        self.tableView = tableView
        self.refreshControl = refreshControl
        
        source
            .filter { $0 > 0 }
            .start {
                data in
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                    self.refreshControl?.endRefreshing()
                })
            }
    }
}
