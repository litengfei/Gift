//
//  API.swift
//  Git@OSC
//
//  Created by AeternChan on 9/6/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import Foundation

struct API {
    static let prefix = "https://git.oschina.net/api/v3/"
    
    static let login = prefix + "session"
    static let projects = prefix + "projects/"
    static let events = prefix + "events/"
    
    static let repo = "/repository/"
    static let files = repo + "files"
    static let commits = repo + "commits/"
    
    static let readme = "readme"
    static let issues = "/issues/"
    static let pullRequest = "pull_requests"
    
    static let notes = "/notes/"
    static let diff = "/diff"

}