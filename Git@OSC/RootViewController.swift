//
//  RootViewController.swift
//  Git@OSC
//
//  Created by AeternChan on 8/10/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit

class RootViewController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        
        AccountManager.loadUserInformation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        if viewController === viewControllers?[1] {
            
            if AccountManager.user == nil {
                self.performSegueWithIdentifier("PresentLoginView", sender: nil)
                return false
            }
        }
        
        return true
    }
    
    @IBAction func dismissToRoot(segue: UIStoryboardSegue) {}
    
}
