//
//  PullRequest.swift
//  Git@OSC
//
//  Created by AeternChan on 9/19/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import SwiftyJSON
import Timepiece

class PullRequest: NSObject {
    
    var id: Int
    var iid: Int
    var targetBranch: String
    var sourceBranch: String
    var projectID: Int
    var title: String
    var state: String
    var createdAt: NSDate!
    
    var author: User
    var assignee: User?
    
    init(_ json: JSON) {
        id = json["id"].intValue
        iid = json["iid"].intValue
        targetBranch = json["target_branch"].stringValue
        sourceBranch = json["source_branch"].stringValue
        projectID = json["project_id"].intValue
        title = json["title"].stringValue
        state = json["state"].stringValue
        createdAt = NSDate.dateFromString(json["created_at"].stringValue)
        
        author = User(json["author"])
        assignee = User(json["assignee"])
    }
}
