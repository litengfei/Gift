//
//  User.swift
//  Git@OSC
//
//  Created by AeternChan on 5/30/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    var id: Int
    var name: String
    var userName: String
    var avatarURL: NSURL?
    
    var privateToken: String?
    
    var followersCount: Int?
    var followingCount: Int?
    var starredCount: Int?
    var watchedCount: Int?
    
    override init() {
        id = 0
        name = ""
        userName = ""
        
        super.init()
    }
    
    init(_ json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        userName = json["username"].stringValue
        avatarURL = json["new_portrait"].URL
        
        privateToken = json["private_token"].string
        
        let count = json["follow"]
        followersCount = count["followers"].int
        followingCount = count["following"].int
        starredCount = count["starred"].int
        watchedCount = count["watched"].int
        
        super.init()
    }
    
    init(name: String) {
        id = 0
        self.name = name
        userName = name
        avatarURL = NSURL(string: "http://secure.gravatar.com/avatar/\(name.md5())?s=40&d=mm")
        
        super.init()
    }
}
