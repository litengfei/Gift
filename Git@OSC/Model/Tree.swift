//
//  Tree.swift
//  Git@OSC
//
//  Created by AeternChan on 8/21/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import SwiftyJSON

class Tree: NSObject {
    
    var id: String
    var name: String
    var type: String
    var mode: Int           // http://stackoverflow.com/questions/737673/how-to-read-the-mode-field-of-git-ls-trees-output
    
    init(_ json: JSON) {
        id = json["id"].stringValue
        name = json["name"].stringValue
        type = json["type"].stringValue
        mode = json["mode"].intValue
    }
}
