//
//  Note.swift
//  Git@OSC
//
//  Created by AeternChan on 10/10/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import SwiftyJSON

class Note: NSObject {
    
    var id: Int
    var body: String
    
    var author: User
    
    var createdAt: NSDate!
//    var target
    
    init(_ json: JSON) {
        id = json["id"].intValue
        body = json["body"].stringValue
        
        author = User(json["author"])
        
        createdAt = NSDate.dateFromString(json["created_at"].stringValue)
    }
}
