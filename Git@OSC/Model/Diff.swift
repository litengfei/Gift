//
//  Diff.swift
//  Git@OSC
//
//  Created by AeternChan on 10/15/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import SwiftyJSON

class Diff: NSObject {
    
    var diff: String
    
    var newPath: String
    var oldPath: String
    
    var aMode: String
    var bMode: String
    
    var isAdded: Bool
    var isRenamed: Bool
    var isRemoved: Bool
    
    var type: String
    
    
    init(_ json: JSON) {
        diff = json["diff"].stringValue
        
        newPath = json["new_path"].stringValue
        oldPath = json["old_path"].stringValue
        
        aMode = json["a_mode"].stringValue
        bMode = json["b_mode"].stringValue
        
        isAdded = json["new_file"].boolValue
        isRenamed = json["renamed_file"].boolValue
        isRemoved = json["deleted_file"].boolValue
        
        type = json["type"].stringValue
    }
    
}
