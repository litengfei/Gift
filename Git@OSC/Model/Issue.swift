//
//  Issue.swift
//  Git@OSC
//
//  Created by AeternChan on 9/6/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import SwiftyJSON
import Timepiece

class Issue: NSObject {
    
    var id: Int
    var iid: Int
    var projectID: Int
    
    var title: String
    var issueDescription: String
    var author: User
    
//    var labels: [String]
    var state: String
    var createdAt: NSDate!
    var updatedAt: NSDate!
    
    init(_ json: JSON) {
        id = json["id"].intValue
        iid = json["iid"].intValue
        projectID = json["project_id"].intValue
        
        title = json["title"].stringValue
        issueDescription = json["description"].stringValue
        author = User(json["author"])
        
        state = json["state"].stringValue
        createdAt = NSDate.dateFromString(json["created_at"].stringValue)
        updatedAt = NSDate.dateFromString(json["updated_at"].stringValue)
    }
    
}
