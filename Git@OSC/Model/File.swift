//
//  File.swift
//  Git@OSC
//
//  Created by AeternChan on 8/21/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import SwiftyJSON

class File: NSObject {
   
    var name: String
    var path: String
    var size: Int
    var content: String
    var ref: String
    var blobID: Int
    var commitID: Int

    init(_ json: JSON) {
        name = json["file_name"].stringValue
        path = json["file_path"].stringValue
        
        size = json["size"].intValue
        content = json["content"].stringValue
        
        ref = json["ref"].stringValue
        blobID = json["blob_id"].intValue
        commitID = json["commit_id"].intValue
    }
}
