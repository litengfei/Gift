//
//  Project.swift
//  Git@OSC
//
//  Created by AeternChan on 5/27/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import SwiftyJSON
import Timepiece

class Project: NSObject {
    
    var id: Int
    var name: String
    var projectDescription: String?
    var createdDate: NSDate!
    var lastPushDate: NSDate?
    var defaultBranch: String?
    var path: String
    
    var stared: Bool
    var watched: Bool
    
    var forksCount: Int
    var starsCount: Int
    var watchesCount: Int
    
    var language: String
    
    var owner: User
    var isPublic: Bool
    var parentID: Int?

    //    var path: String
    //    var pathWithNamespace: String
    //    var isPullRequestEnabled: Bool
    //    var isWikiEnabled: Bool
    //    var parentID: Int?
    //
    //    var relation: Bool
    //    var realPath: String
    //    var svnURL: String?
    //
    //    var namespace: Namespace?
    //
    //    struct Namespace {
    //        var updatedTime: String
    //        var id: Int
    //        var createdTime: String
    //        var namespaceDescription: String
    //        var path: String
    //        var name: String
    //        var ownerID: Int
    //    }
    
    
    init(_ json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        projectDescription = json["description"].string?.stringByTrimmingCharactersInSet(.whitespaceAndNewlineCharacterSet())
        
        createdDate = NSDate.dateFromString(json["created_at"].stringValue)
        
        let lastPushDateStr = json["last_push_at"].string
        if lastPushDateStr != nil {
            lastPushDate = NSDate.dateFromString(lastPushDateStr!)
        }
        
        path = json["path"].stringValue
        isPublic = json["public"].boolValue
        parentID = json["parent_id"].int
        
        stared = json["stared"].boolValue
        watched = json["watched"].boolValue
        
        language = json["language"].stringValue
        forksCount = json["forks_count"].intValue
        starsCount = json["stars_count"].intValue
        watchesCount = json["watches_count"].intValue
        
        owner = User(json["owner"])
    }
}
