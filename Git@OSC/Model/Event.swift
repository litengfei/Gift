//
//  Event.swift
//  Git@OSC
//
//  Created by AeternChan on 12/29/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import SwiftyJSON

enum Action: Int {
    case CREATED = 1,
         UPDATED,
         CLOSED,
         REOPENED,
         PUSHED,
         COMMENTED,
         MERGED,
         JOINED,
         LEFT,
         FORKED
}

class EventObject {
    var note: Note?
    var issue: Issue?
    var pullRequest: PullRequest?
    var title: String
    
    init(_ json: JSON) {
        note = Note(json["note"])
        issue = Issue(json["issue"])
        pullRequest = PullRequest(json["pull_request"])
        
        if json["note"] != nil {
            title = "评论"
        } else if json["issue"] != nil {
            title = "issue #\(issue!.iid)"
        } else if json["pull_request"] != nil {
            title = "pull request #\(pullRequest!.iid)"
        } else {
            title = ""
        }
    }
}

class Event: NSObject {

    var id: Int
    var title: String
    var author: User
    
    var project: Project
    var ref: String
    
    var action: Action
    var actionDescription: String
    
    var createdAt: NSDate!
    var updatedAt: NSDate!
    
    var totalCommitsCount: Int?
    var commits: [Commit]?
    
    var eventObject: EventObject
    
    init(_ json: JSON) {
        id = json["id"].intValue
        title = json["title"].stringValue
        author = User(json["author"])
        
        project = Project(json["project"])
        ref = json["data"]["ref"].stringValue.componentsSeparatedByString("/").last!
        
        eventObject = EventObject(json["events"])
        
        action = Action(rawValue: json["action"].intValue)!
        actionDescription = "\(author.name) "
        switch action {
        case .CREATED:
            actionDescription.appendContentsOf("在项目 \(project.name) 创建了 \(eventObject.title)")
        case .UPDATED:
            actionDescription.appendContentsOf("更新了项目 \(project.name)")
        case .CLOSED:
            actionDescription.appendContentsOf("关闭了项目 \(project.name) 的 \(eventObject.title)")
        case .REOPENED:
            actionDescription.appendContentsOf("重新打开了项目 \(project.name) 的 \(eventObject.title)")
        case .PUSHED:
            actionDescription.appendContentsOf("推送到了项目 \(project.name) 的 \(ref) 分支")
        case .COMMENTED:
            actionDescription.appendContentsOf("评论了项目 \(project.name) 的 \(eventObject.title)")
        case .MERGED:
            actionDescription.appendContentsOf("接受了项目 \(project.name) 的 \(eventObject.title)")
        case .JOINED:
            actionDescription.appendContentsOf("加入了项目 \(project.name)")
        case .LEFT:
            actionDescription.appendContentsOf("离开了项目 \(project.name)")
        case .FORKED:
            actionDescription.appendContentsOf("Fork了项目 \(project.name)")
        }
        
        createdAt = NSDate.dateFromString(json["created_at"].stringValue)
        updatedAt = NSDate.dateFromString(json["updated_at"].stringValue)
        
        totalCommitsCount = json["total_commits_count"].int
        commits = json["commits"].arrayValue.map{Commit($0)}
    }
}
