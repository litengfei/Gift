//
//  Commit.swift
//  Git@OSC
//
//  Created by AeternChan on 9/11/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import SwiftyJSON

class Commit: NSObject {
   
    var id: String
    var shortID: String
    var title: String
    
    var author: User?
    var authorEmail: String
    var authorName: String
    
    var createdAt: NSDate!
    
    
    init(_ json: JSON) {
        
        id = json["id"].stringValue
        shortID = json["short_id"].stringValue[0..<7]
        title = json["title"].stringValue
        
        authorName = json["author_name"].stringValue
        authorEmail = json["author_email"].stringValue
        author = User(json["author"])
        if author?.avatarURL == nil {
            author = User(name: authorName)
        }
        
        createdAt = NSDate.dateFromString(json["created_at"].stringValue)
    }
    
}
