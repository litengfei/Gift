//
//  FilesViewController.swift
//  Git@OSC
//
//  Created by AeternChan on 8/20/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FilesViewController: UITableViewController {
    
    var namespace: String!
    var path = ""
    var files = [Tree]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request(.GET, "http://git.oschina.net/api/v3/projects/\(namespace)/repository/tree",
                          parameters: ["path": path])
            .responseJSON { response in
                
                if response.result.isSuccess {
                    let json = JSON(response.result.value!)
                    
                    for (_, treeJSON) in json {
                        let tree = Tree(treeJSON)
                        self.files.append(tree)
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.tableView.reloadData()
                    })
                } else {
                    
                }
        }
    }

    

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("FileCell", forIndexPath: indexPath) as! FileCell
        let file = files[indexPath.row]
        
        cell.entity = file

        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let file = files[indexPath.row]
        
        if file.type == "tree" {
            let destinationVC = self.storyboard?.instantiateViewControllerWithIdentifier(file.type) as! FilesViewController
            destinationVC.namespace = self.namespace
            destinationVC.path = path + file.name + "/"
            self.navigationController!.pushViewController(destinationVC, animated: true)
        } else {
            let destinationVC = self.storyboard?.instantiateViewControllerWithIdentifier(file.type) as! FileContentViewController
            destinationVC.url = API.projects + namespace + "/" + API.files
            destinationVC.path = path + file.name + "/"
            self.navigationController!.pushViewController(destinationVC, animated: true)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
