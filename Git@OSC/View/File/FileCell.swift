//
//  FileCell.swift
//  Git@OSC
//
//  Created by AeternChan on 8/21/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit

class FileCell: UITableViewCell {
    
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    var entity: Tree! {
        didSet {
            nameLabel.text = entity.name
            
            switch entity.type {
            case "tree":
                if entity.mode == 160000 {
                    iconLabel.text = Octicons.FileSubmodule.rawValue
                } else {
                    iconLabel.text = Octicons.FileDirectory.rawValue
                }
                
                iconLabel.textColor = UIColor(rgba: "#80A6CD")
            default:
                iconLabel.text = Octicons.FileText.rawValue
                iconLabel.textColor = UIColor(rgba: "#767676")
            }
        }
    }

}
