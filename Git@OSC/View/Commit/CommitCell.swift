//
//  CommitCell.swift
//  Git@OSC
//
//  Created by AeternChan on 9/11/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Kingfisher

class CommitCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    var entity: Commit! {
        didSet {
            avatar.kf_setImageWithURL(entity.author!.avatarURL!)
            messageLabel.text = entity.title
            infoLabel.text = "\(entity.shortID) - \(entity.author!.name) - \(entity.createdAt.prettify())"
        }
    }
}
