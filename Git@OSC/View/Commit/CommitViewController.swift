
//
//  CommitViewController.swift
//  Git@OSC
//
//  Created by AeternChan on 10/13/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit

class CommitViewController: UITableViewController {
    
    var viewModel: CommitViewModel!
    
    @IBOutlet var titleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        
        titleLabel.text = viewModel.commit.title
        
        viewModel.diffTypes.producer
            .filter { $0.count > 0 }
            .start {
                data in
                    self.tableView.reloadData()
        }
        
        viewModel.fetchDiff()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return viewModel.notes.count > 0 ? 2 : 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return viewModel.diffTypes.value.count
        } else {
            return viewModel.notes.count
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("Diff", forIndexPath: indexPath)
            cell.textLabel?.font = UIFont(name: "octicons", size: 18)
            
            switch viewModel.diffTypes.value[indexPath.row] {
                case .All: cell.textLabel?.text = "\(Octicons.Diff) \(viewModel.allFiles.count)"
                case .Added: cell.textLabel?.text = "\(Octicons.DiffAdded) \(viewModel.addedFiles.count) added"
                case .Removed: cell.textLabel?.text = "\(Octicons.DiffRemoved) \(viewModel.removedFiles.count) removed"
                case .Renamed: cell.textLabel?.text = "\(Octicons.DiffRenamed) \(viewModel.renamedFiles.count) renamed"
                case .Modified: cell.textLabel?.text = "\(Octicons.DiffModified) \(viewModel.modifiedFiles.count) modified"
            }
            
            return cell
        } else {
            return tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let indexPath = tableView.indexPathForSelectedRow!
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var diffs: [Diff]
        switch viewModel.diffTypes.value[indexPath.row] {
            case .All: diffs = viewModel.allFiles
            case .Added: diffs = viewModel.addedFiles
            case .Removed: diffs = viewModel.removedFiles
            case .Renamed: diffs = viewModel.renamedFiles
            case .Modified: diffs = viewModel.modifiedFiles
        }
        
        let diffVC = segue.destinationViewController as! DiffViewController
        diffVC.diffs = diffs
    }

}
