//
//  DiffViewController.swift
//  Git@OSC
//
//  Created by AeternChan on 12/28/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit

class DiffCell: UITableViewCell {
    @IBOutlet var diffView: UITextView!
}

class DiffViewController: UITableViewController {
    
    var diffs = [Diff]()
    
    convenience init(diffs: [Diff]) {
        self.init()
        self.diffs = diffs
    }
    
    override func viewDidLoad() {
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return diffs.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Diff", forIndexPath: indexPath) as! DiffCell
        let diff = diffs[indexPath.section]
        
        cell.diffView.text = diff.diff
        
        return cell
    }
}
