//
//  EventCell.swift
//  Git@OSC
//
//  Created by AeternChan on 1/3/16.
//  Copyright © 2016 oschina. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {
    
    @IBOutlet var avatarView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var contentTextView: UITextView!
    @IBOutlet var timeLabel: UILabel!
    
    var entity: Event! {
        didSet {
            titleLabel.text = entity.title
            
            avatarView.kf_setImageWithURL(entity.author.avatarURL!)
            
            titleLabel.text = entity.actionDescription
            timeLabel.text = entity.updatedAt.prettify()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
