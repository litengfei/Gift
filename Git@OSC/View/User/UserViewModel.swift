//
//  UserViewModel.swift
//  Git@OSC
//
//  Created by AeternChan on 12/31/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit

class UserViewModel: NSObject {
    
    var user: User
    var eventsViewModel: EventsViewModel
    var projectsViewModel: ProjectsViewModel
    var starredProjectsViewModel: ProjectsViewModel
    var watchedProjectsViewModel: ProjectsViewModel
    
    override init() {
        user = AccountManager.user!
        eventsViewModel = EventsViewModel(privateToken: user.privateToken!)
        projectsViewModel = ProjectsViewModel(userID: user.id)
        starredProjectsViewModel = ProjectsViewModel(userID: user.id)
        watchedProjectsViewModel = ProjectsViewModel(userID: user.id)
        
        super.init()
    }
}
