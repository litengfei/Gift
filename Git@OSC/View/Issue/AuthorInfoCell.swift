//
//  AuthorInfoCell.swift
//  Git@OSC
//
//  Created by AeternChan on 9/27/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import Kingfisher

class AuthorInfoCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    
    func setContent(avatarURL: NSURL, info: String) {
        avatar.kf_setImageWithURL(avatarURL, placeholderImage: UIImage(named: "default-avatar"))
        infoLabel.text = info
    }
    
}
