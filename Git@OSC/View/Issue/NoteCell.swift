//
//  NoteCell.swift
//  Git@OSC
//
//  Created by AeternChan on 10/10/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import Kingfisher

class NoteCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    var entity: Note! {
        didSet {
            avatar.kf_setImageWithURL(entity.author.avatarURL!, placeholderImage: UIImage(named: "default-avatar"))
            nameLabel.text = entity.author.name
            dateLabel.text = entity.createdAt.prettify()
            
            do {
                try contentLabel.attributedText = NSAttributedString(
                    data: entity.body.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: false)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil
                )
            } catch {
                print(error)
            }
            
        }
    }

}
