//
//  WebViewCell.swift
//  Git@OSC
//
//  Created by AeternChan on 9/27/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import WebKit
import Mustache

class WebViewCell: UITableViewCell {
    
//    var webView = WKWebView()
    @IBOutlet weak var webView: UIWebView!
    var html: String! {
        didSet {
            let data = [
                "content": html,
                "style": NSUserDefaults.standardUserDefaults().objectForKey("style") as? String
            ]
            
            let template = try! Template(named: "description")
            let rendering = try! template.render(Box(data))
            webView.loadHTMLString(rendering, baseURL: NSBundle.mainBundle().resourceURL)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
//        webView.frame = contentView.bounds
//        webView.scrollView.scrollEnabled = false
//        self.contentView.addSubview(webView)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
