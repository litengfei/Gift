//
//  IssueCell.swift
//  Git@OSC
//
//  Created by AeternChan on 9/6/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Timepiece

class IssueCell: UITableViewCell {
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    var entity: Issue! {
        didSet {
            if entity.state == "closed" {
                statusLabel.textColor = UIColor(rgba:"#BD2C00")
                statusLabel.text = Octicons.IssueClosed.rawValue
            } else {
                statusLabel.textColor = UIColor(rgba: "#6CC644")
                statusLabel.text = Octicons.IssueOpened.rawValue
            }
            
            titleLabel.text = entity.title
            infoLabel.text = "#\(entity.iid) by \(entity.author.name) - \(entity.createdAt.prettify())"
        }
    }
}
