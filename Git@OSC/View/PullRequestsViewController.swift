//
//  PullRequestsViewController.swift
//  Git@OSC
//
//  Created by AeternChan on 9/19/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import ReactiveCocoa

class PullRequestsViewController: UITableViewController {
    
    var viewModel: PullRequestsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        viewModel.page.producer
            .filter { $0 > 0 }
            .start {
                data in
                self.tableView.reloadData()
        }
        
        viewModel.fetchPullRequests()
    }


    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.pullRequests.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PullRequestCell", forIndexPath: indexPath) as! PullRequestCell
        
        cell.entity = viewModel.pullRequests[indexPath.row]

        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
