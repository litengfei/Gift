//
//  PullRequestCell.swift
//  Git@OSC
//
//  Created by AeternChan on 9/19/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit

class PullRequestCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    var entity: PullRequest! {
        didSet {
            if entity.state == "opened" {
                statusLabel.textColor = UIColor(rgba:"#6CC644")
            } else if entity.state == "merged" {
                statusLabel.textColor = UIColor(rgba: "#6E5494")
            } else {
                statusLabel.textColor = UIColor(rgba: "#BD2C00")
            }
            
            titleLabel.text = entity.title
            infoLabel.text = "#\(entity.iid) by \(entity.author.name) - \(entity.createdAt.prettify())"
        }
    }

}
