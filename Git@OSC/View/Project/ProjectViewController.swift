//
//  ProjectViewController.swift
//  Git@OSC
//
//  Created by AeternChan on 8/13/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Kingfisher

class ProjectViewController: UITableViewController {
    
    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var repoIcon: UILabel!
    
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var watchButton: UIButton!
    @IBOutlet weak var starCountLabel: UILabel!
    @IBOutlet weak var watchCountLabel: UILabel!
    
    @IBOutlet weak var lastPushDateLabel: UILabel!
    
    @IBOutlet weak var ownerAvatarView: UIImageView!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var languageLabel: UILabel!
    
    
    var viewModel = ProjectViewModel()
    var bindingHelper: TableViewBindingHelper!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.clearsSelectionOnViewWillAppear = true
        
        viewModel.fetchProject()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: viewModel.projectModel.project.name, style: .Plain, target: nil, action: nil)
        
        ownerAvatarView.kf_setImageWithURL(viewModel.projectModel.ownerAvatarURL)
        ownerNameLabel.text = viewModel.projectModel.project.owner.name
        
        let starButtonTittle = viewModel.projectModel.project.stared ? "\(Octicons.Star) Unstar" : "\(Octicons.Star) Star"
        let watchButtonTitlle = viewModel.projectModel.project.watched ? "\(Octicons.Eye) Unwatch" : "\(Octicons.Eye) Watch"
        starButton.setTitle(starButtonTittle, forState: .Normal)
        watchButton.setTitle(watchButtonTitlle, forState: .Normal)
        starCountLabel.text = viewModel.projectModel.project.starsCount.description
        watchCountLabel.text = viewModel.projectModel.project.watchesCount.description
        
        descriptionLabel.text = viewModel.projectModel.project.projectDescription
        
        repoIcon.text = viewModel.repoIcon
        projectNameLabel.text = viewModel.projectModel.project.name
        lastPushDateLabel.text = "最后提交于\(viewModel.projectModel.lastUpdateTime)"
        
        languageLabel.text = viewModel.projectModel.language
    }
    
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ShowFiles" {
            
            let filesVC = segue.destinationViewController as! FilesViewController
            filesVC.namespace = viewModel.projectModel.namespace
            
        } else if segue.identifier == "ShowReadme" {
            
            let fileVC = segue.destinationViewController as! FileContentViewController
            fileVC.url = "\(API.projects)\(viewModel.projectModel.namespace)/\(API.readme)"
            fileVC.isReadme = true
            fileVC.path = ""
            
        } else if segue.identifier == "ShowIssues" {
            
            let issuesVC = segue.destinationViewController as! IssuesViewController
            issuesVC.viewModel = viewModel.issusViewModel()
            
        } else if segue.identifier == "ShowCommits" {
            
            let commitsVC = segue.destinationViewController as! CommitsViewController
            commitsVC.viewModel = viewModel.commitsViewModel()
        } else if segue.identifier == "ShowPullRequests" {
            
            let pullRequestsVC = segue.destinationViewController as! PullRequestsViewController
            pullRequestsVC.viewModel = viewModel.pullRequestsViewModel()
        }
        
    }
    
}
