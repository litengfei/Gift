//
//  ProjectCell.swift
//  Git@OSC
//
//  Created by AeternChan on 5/30/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class ProjectCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var watchesLabel: UILabel!
    
    @IBOutlet weak var descriptionBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var languageRightConstraint: NSLayoutConstraint!
    
    var entity: ProjectModel! {
        didSet {
            titleLabel.text = entity.title
            
            avatarView.kf_setImageWithURL(entity.ownerAvatarURL)
            
            descriptionLabel.text = entity.projectDescription
            if (descriptionLabel.text!).characters.count == 0 {
                descriptionBottomConstraint.constant = 0
            }
            
            languageLabel.text = "\(entity.language)"
            if (languageLabel.text!).characters.count == 0 {
                languageRightConstraint.constant = 0
            }
            
            forksLabel.text = "\(Octicons.GitBranch) \(entity.forksCount)"
            starsLabel.text = "\(Octicons.Star) \(entity.starsCount)"
            watchesLabel.text = "\(Octicons.Eye) \(entity.watchesCount)"
        }
    }
    
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        contentView.setNeedsUpdateConstraints()
        contentView.updateConstraintsIfNeeded()
    }
    
    
    override func prepareForReuse() {
        languageRightConstraint.constant = 20
        descriptionBottomConstraint.constant = 8
    }
}
