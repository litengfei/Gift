//
//  CommitsViewModel.swift
//  Git@OSC
//
//  Created by AeternChan on 9/11/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ReactiveCocoa

class CommitsViewModel: NSObject {
    
    var commits = [Commit]()
    var page = MutableProperty<Int>(0)
    var namespace: String!
    
    
    init(_ namespace: String) {
        self.namespace = namespace
        super.init()
    }
    
    func fetchCommits() {
        Alamofire.request(.GET, API.projects + namespace + API.commits)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    let json = JSON(response.result.value!)
                    
                    for (_, commitJSON) in json {
                        let commit = Commit(commitJSON)
                        self.commits.append(commit)
                    }
                    
                    self.page.value += 1
                } else {
                    
                }
        }
    }
    
    func commitViewModel(indexPath: NSIndexPath) -> CommitViewModel {
        let viewModel = CommitViewModel()
        viewModel.namespace = namespace
        viewModel.commit = commits[indexPath.row]
        
        return viewModel
    }
    
}
