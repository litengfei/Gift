//
//  EventsViewModel.swift
//  Git@OSC
//
//  Created by AeternChan on 12/31/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ReactiveCocoa

class EventsViewModel: NSObject {
    
    var events = [Event]()
    var privateToken: String?
    var userID: Int?
    
    var page = MutableProperty<Int>(0)
    
    convenience init(privateToken: String) {
        self.init()
        self.privateToken = privateToken
    }
    
    convenience init(userID: Int) {
        self.init()
        self.userID = userID
    }
    
    func fetchEvents() {
        Alamofire.request(.GET, API.events, parameters: ["private_token": privateToken!]).responseJSON {
            response in
            if response.result.isSuccess {
                let json = JSON(response.result.value!)
                
                self.events.appendContentsOf(json.arrayValue.map{Event($0)})
                
                self.page.value += 1
            }
        }
    }
}
