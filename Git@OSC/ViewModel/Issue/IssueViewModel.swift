//
//  IssueViewModel.swift
//  Git@OSC
//
//  Created by AeternChan on 9/26/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ReactiveCocoa

class IssueViewModel: NSObject {
    
    var namespace: String!
    var issue: Issue!
    var notes = [Note]()
    var page = MutableProperty<Int>(0)
    
    func fetchNotes() {
        Alamofire.request(.GET, API.projects + namespace + API.issues + "\(issue.id)" + API.notes)
            .responseJSON {
                response in
                
                if response.result.isSuccess {
                    let json = JSON(response.result.value!)
                    
                    for (_, noteJSON) in json {
                        let note = Note(noteJSON)
                        self.notes.append(note)
                    }
                    
                    self.page.value += 1
                } else {
                    
                }
        }
    }
}
