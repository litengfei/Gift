//
//  IssuesViewModel.swift
//  Git@OSC
//
//  Created by AeternChan on 9/7/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ReactiveCocoa

class IssuesViewModel: NSObject {
    
    var issues = [Issue]()
    var page = MutableProperty<Int>(0)
    var namespace: String!
    
    
    init(_ namespace: String) {
        self.namespace = namespace
        super.init()
    }
    
    func fetchIssues() {
        Alamofire.request(.GET, API.projects + namespace + API.issues)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    let json = JSON(response.result.value!)
                    
                    for (_, issueJSON) in json {
                        let issue = Issue(issueJSON)
                        self.issues.append(issue)
                    }
                    
                    self.page.value += 1
                } else {
                    
                }
        }
    }
    
    
    func issueViewModel(indexPath: NSIndexPath) -> IssueViewModel {
        let viewModel = IssueViewModel()
        viewModel.namespace = namespace
        viewModel.issue = issues[indexPath.row]
        
        return viewModel
    }
}
