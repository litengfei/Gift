//
//  ProjectModel.swift
//  Git@OSC
//
//  Created by AeternChan on 5/29/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Timepiece

class ProjectModel: NSObject {
    
    var project: Project
    var title: String
    var ownerAvatarURL: NSURL
    var projectDescription: String?
    var language: String
    var forksCount: Int
    var starsCount: Int
    var watchesCount: Int
    var namespace: String
    
    init(_ project: Project) {
        self.project = project
        
        title = "\(project.owner.name) / \(project.name)"
        ownerAvatarURL = project.owner.avatarURL!
        projectDescription = project.projectDescription
        language = project.language
        
        forksCount = project.forksCount
        starsCount = project.starsCount
        watchesCount = project.watchesCount
        
        namespace = "\(project.owner.userName)%2F\(project.path)".stringByReplacingOccurrencesOfString(".", withString: "+")
    }
    
    
    var lastUpdateTime: String {
        let lastUpdateDate = (project.lastPushDate ?? project.createdDate)!
        
        return lastUpdateDate.prettify()
    }
}
