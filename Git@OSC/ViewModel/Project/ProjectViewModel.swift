//
//  ProjectViewModel.swift
//  Git@OSC
//
//  Created by AeternChan on 8/13/15.
//  Copyright (c) 2015 oschina. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProjectViewModel: NSObject {
    
    var projectModel: ProjectModel!
    
    func fetchProject() {
        
        Alamofire.request(.GET, API.projects + projectModel.namespace)
            .responseJSON { response in
                
                if response.result.isSuccess {
                    let project = Project(JSON(response.result.value!))
                    self.projectModel = ProjectModel(project)
                } else {
                    
                }
        }
    }
    
    
    func toggleStar() {
        // 点击后使按钮处于Disabled状态，文字变灰
        // 发起请求
        // 请求成功后，修改按钮tittle，否则恢复原tittle
    }
    
    func toggleWatch() {
        // 同toggleStar
    }
    
    var repoIcon: String {
        if !projectModel.project.isPublic {
            return Octicons.RepoForked.rawValue
        } else if projectModel.project.parentID != nil {
            return Octicons.RepoClone.rawValue
        } else {
            return Octicons.Repo.rawValue
        }
    }
    
    
    // MARK: - Child ViewModel
    
    func issusViewModel() -> IssuesViewModel {
        return IssuesViewModel(projectModel.namespace)
    }
    
    func commitsViewModel() -> CommitsViewModel {
        return CommitsViewModel(projectModel.namespace)
    }
    
    func pullRequestsViewModel() -> PullRequestsViewModel {
        return PullRequestsViewModel(projectModel.namespace)
    }
}
