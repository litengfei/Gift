//
//  PullRequestsViewModel.swift
//  Git@OSC
//
//  Created by AeternChan on 9/19/15.
//  Copyright © 2015 oschina. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ReactiveCocoa

class PullRequestsViewModel: NSObject {
    
    var pullRequests = [PullRequest]()
    var page = MutableProperty<Int>(0)
    var namespace: String!
    
    init(_ namespace: String) {
        self.namespace = namespace
        super.init()
    }
    
    func fetchPullRequests() {
        Alamofire.request(.GET, API.projects + namespace + "/" + API.pullRequest)
            .responseJSON {
                response in
                
                if response.result.isSuccess {
                    let json = JSON(response.result.value!)
                    
                    for (_, pullRequestJSON) in json {
                        self.pullRequests.append(PullRequest(pullRequestJSON))
                    }
                    
                    self.page.value += 1
                } else {
                    
                }
        }
    }
}
